# Long-read data analysis workshop

This repository contains the materials for the __Long-read data analysis workshop__ held at the UNSW (Sydney, Australia) on the 27-28 September 2018.

A combined presentation of the contents can be found at F1000Research as a permanent record:

* [https://f1000research.com/slides/7-1595](https://f1000research.com/slides/7-1595)

For the raw and pre-processed data needed for the tutorials, please visit the Zenodo repository:

* https://doi.org/10.5281/zenodo.1434006
